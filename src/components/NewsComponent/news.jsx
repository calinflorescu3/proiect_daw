import React from "react";
import { Card, CardHeader, CardBody, CardText, CardTitle } from "reactstrap";
import "./news.css";

export default function News() {
  return (
    <div className={"news-container"}>
      <div className={"news-header"}>Noutati despre proiect</div>
      <div className={"news-body"}>
        <Card className={"news-card"}>
          <CardHeader className={"news-card-title"}>
            Prima versiune lansata - 18.03.2020
          </CardHeader>
          <CardBody className={"news-card-body"}>
            <CardTitle>A aparut prima versiune a site-ului</CardTitle>
            <CardText>
              Salut cititorule, avem o veste buna pentru tine. Dupa multa munca,
              prima versiune a site-ului este acum utilizabila. Te invitam sa
              testezi paginile site-ului si speram sa apreciezi design-ul
              minimalist al acestuia.
            </CardText>
          </CardBody>
        </Card>
        <Card className={"news-card"}>
          <CardHeader className={"news-card-title"}>
            Model stire - 18.03.2020
          </CardHeader>
          <CardBody className={"news-card-body"}>
            <CardTitle>Titlu</CardTitle>
            <CardText>Descriere</CardText>
          </CardBody>
        </Card>
        <Card className={"news-card"}>
          <CardHeader className={"news-card-title"}>
            Model stire - 18.03.2020
          </CardHeader>
          <CardBody className={"news-card-body"}>
            <CardTitle>Titlu</CardTitle>
            <CardText>Descriere</CardText>
          </CardBody>
        </Card>
        <Card className={"news-card"}>
          <CardHeader className={"news-card-title"}>
            Model stire - 18.03.2020
          </CardHeader>
          <CardBody className={"news-card-body"}>
            <CardTitle>Titlu</CardTitle>
            <CardText>Descriere</CardText>
          </CardBody>
        </Card>
      </div>
    </div>
  );
}
