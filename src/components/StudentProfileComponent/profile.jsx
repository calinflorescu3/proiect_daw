import React from "react";
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle
} from "reactstrap";
import "./profile.css";

const Example = props => {
  return (
    <div className={"profile-container"}>
      <div className={"news-header"}>Despre mine</div>
      <div className={"contact-container"}>
        <Card className={"profile-card"}>
          <CardImg
            top
            width="100%"
            src="/Poza.jpg"
            alt="Card image cap"
            className={"image"}
          />
          <CardBody>
            <CardTitle>Calin Florescu</CardTitle>
            <CardSubtitle>Domenii de interes</CardSubtitle>
            <CardText>
              Web Development, algoritmi, eficientizare, management
            </CardText>
          </CardBody>
        </Card>
      </div>
    </div>
  );
};

export default Example;
