import React from "react";
import { Card, CardHeader, CardBody, CardTitle } from "reactstrap";
import "./coordonator.css";

export default function coordonator() {
  return (
    <div className={"news-container"}>
      <div className={"news-header"}>
        Informatii despre profesorul coordonator
      </div>
      <div className={"coordonator-container"}>
        <Card className="text-center">
          <CardHeader className={"contact-header"}>Nume</CardHeader>
          <CardBody>
            <CardTitle className={"contact-text"}>Nume prenume</CardTitle>
          </CardBody>
        </Card>

        <Card className="text-center">
          <CardHeader className={"contact-header"}>Functie</CardHeader>
          <CardBody>
            <CardTitle className={"contact-text"}>
              Functia profesorului
            </CardTitle>
          </CardBody>
        </Card>

        <Card className="text-center">
          <CardHeader className={"contact-header"}>
            Directii de cercetare
          </CardHeader>
          <CardBody>
            <CardTitle className={"contact-text"}>
              Directiile de cercetare ale profesorului
            </CardTitle>
          </CardBody>
        </Card>
      </div>
    </div>
  );
}
