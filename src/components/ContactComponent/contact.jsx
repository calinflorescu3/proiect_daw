import React from "react";
import { Card, CardHeader, CardText, CardBody, CardTitle } from "reactstrap";
import "./contact.css";

export default function contact() {
  return (
    <div className={"news-container"}>
      <div className={"news-header"}>Informatii de contact</div>
      <div className={"contact-body"}>
        <Card className="text-center">
          <CardHeader className={"contact-header"}>Email</CardHeader>
          <CardBody>
            <CardTitle className={"contact-text"}>
              calinflorescu3@gmail.com
            </CardTitle>
          </CardBody>
        </Card>

        <Card className="text-center">
          <CardHeader className={"contact-header"}>Adresa</CardHeader>
          <CardBody>
            <CardTitle className={"contact-text"}>
              Strada X, Bloc Y, Apartament Z, oras Cluj-Napoca, judet Cluj
            </CardTitle>
          </CardBody>
        </Card>

        <Card className="text-center">
          <CardHeader className={"contact-header"}>Telefon</CardHeader>
          <CardBody>
            <CardTitle className={"contact-text"}>0751322081</CardTitle>
          </CardBody>
        </Card>
      </div>
    </div>
  );
}
