import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import Profile from "../StudentProfileComponent/profile";
import News from "../NewsComponent/news";
import Project from "../ProjectComponent/project";
import Contact from "../ContactComponent/contact";
import Coordonator from "../CoordonatorComponent/coordonator";
import "./landingPage.css";

export default function landingPageComponent() {
  const [activeScreen, setActiveScreen] = useState("Acasa");

  const homeHandleClick = () => {
    setActiveScreen("Acasa");
  };

  const newsHandleClick = () => {
    setActiveScreen("Noutati");
  };

  const aboutHandleClick = () => {
    setActiveScreen("Despre");
  };

  const profileHandleClick = () => {
    setActiveScreen("Profil");
  };

  const coordonatorHandleClick = () => {
    setActiveScreen("Coordonator");
  };

  const contactHandleClick = () => {
    setActiveScreen("Contact");
  };

  return (
    <div className={"container"}>
      <Row className={"row"}>
        <Col className={"col"} onClick={homeHandleClick}>
          Acasa
        </Col>
        <Col className={"col"} onClick={newsHandleClick}>
          Noutati
        </Col>
        <Col className={"col"} onClick={aboutHandleClick}>
          Despre Lucrare
        </Col>
        <Col className={"col"} onClick={profileHandleClick}>
          Profil Student
        </Col>
        <Col className={"col"} onClick={coordonatorHandleClick}>
          Coordonator
        </Col>
        <Col className={"col"} onClick={contactHandleClick}>
          Contact
        </Col>
      </Row>
      <Row className={"body-container"}>
        {activeScreen === "Acasa" ? (
          <div className={"welcome"}>Bun venit!</div>
        ) : null}
        {activeScreen === "Noutati" ? <News /> : null}
        {activeScreen === "Despre" ? <Project /> : null}
        {activeScreen === "Profil" ? <Profile /> : null}
        {activeScreen === "Coordonator" ? <Coordonator /> : null}
        {activeScreen === "Contact" ? <Contact /> : null}
      </Row>
      <div className={"decor"}></div>
    </div>
  );
}
